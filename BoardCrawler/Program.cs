﻿using log4net;
using log4net.Config;
using System;
using System.Reflection;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Xml;
using BoardCrawlerLib;
using System.Text;

namespace BoardCrawler
{
    class Program
    {
        private static ILog _log = LogManager.GetLogger(typeof(Program));

        static string _storagePath;

        static string _dbServer;
        static string _dbDatabase;
        static string _dbUsername;
        static string _dbPassword;

        static XmlElement _sources;

        static void Main(string[] args)
        {
            try
            {
                var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
                XmlConfigurator.Configure(logRepository, new System.IO.FileInfo("log4net.config"));

                LoadConfig();

                IDBConnector database = new SQLDBConnector();
                //((SQLDBConnector)database).Connect(_dbServer, _dbDatabase, _dbUsername, _dbPassword);
                //string connectionString = localDBConStrBld.ToString();
                //((SQLDBConnector)database).Connect(connectionString);

                database = new JSONStorage(_storagePath);

                if (database.connected)
                {
                    if(args.Count() > 0)
                    {
                        Interface(args, database);
                    }
                    else
                    {
                        Dictionary<IBoardAPI, XmlNode> boards = new Dictionary<IBoardAPI, XmlNode>();

                        for(int i = 0; i < _sources.ChildNodes.Count; i++)
                        {
                            var s = _sources.ChildNodes.Item(i);
                            if (s.Name == "Inkbunny")
                                boards.Add(new IBCrawler(database), s);
                            if (s.Name == "e621")
                                boards.Add(new e6Crawler(database), s);
                        }

                        foreach(var api in boards)
                        {
                            try
                            {
                                if (api.Key.Login(api.Value))
                                {
                                    api.Key.storageTarget = _storagePath;
                                    api.Key.DownloadFavs();
                                    api.Key.Logout();
                                }
                            }
                            catch (Exception ex)
                            {
                                _log.Error(ex.Message + ex.StackTrace);
                                Console.WriteLine(ex.Message + ex.StackTrace);
                            }
                        }                      
                    }

                    database.Close();
                }
                else
                    _log.Warn("Can't connect to Database");

            }
            catch(Exception ex)
            {
                _log.Error(ex.Message + ex.StackTrace);
                Console.WriteLine(ex.Message + ex.StackTrace);
            }
        }

        private static void LoadConfig()
        {
            XmlDocument xmlConfig = new XmlDocument();
            xmlConfig.Load("appsettings.xml");

            var root = xmlConfig["Configuration"];

            _storagePath = root["StoragePath"].InnerText;

            if (root["Database"] != null)
            {
                _dbServer = root["Database"]["Server"].InnerText;
                _dbDatabase = root["Database"]["Database"].InnerText;
                _dbUsername = root["Database"]["Username"].InnerText;
                _dbPassword = root["Database"]["Password"].InnerText;
            }

            _sources = root["Datasources"];
        }

        private static void Interface(string[] command, IDBConnector db)
        {
            string com = command.First();
            switch (com)
            {
                case "-help":
                case "-h":
                    Console.WriteLine("-wipedb - Lösche die Datenbank");
                    break;

                case "-wipedb":
                    db.wipeDB();
                    break;

                case "-select":

                    break;
            }
        }
    }
}