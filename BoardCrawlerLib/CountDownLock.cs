﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace BoardCrawlerLib
{
    class CountDownLock
    {
        object _valLock = new object();
        int _value;

        public int value
        {
            get
            {
                lock(_valLock)
                {
                    return _value;
                }
            }
            private set
            {
                lock (_valLock)
                {
                    _value = value;
                    if (_value == 0)
                        _lock.Set();
                    else
                        _lock.Reset();
                }
            }
        }

        ManualResetEvent _lock = new ManualResetEvent(true);

        public CountDownLock(int val)
        {
            value = val;
        }

        public void CountDown()
        {
            value--;
        }

        public void Await()
        {
            _lock.WaitOne();
        }
    }
}
