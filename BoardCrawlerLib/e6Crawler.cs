﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Text.Json;
using System.Text.Json.Serialization;
using log4net;
using System.Xml;
using System.Net;
using System.Linq;
using System.IO;

namespace BoardCrawlerLib
{
    public class e6Crawler : IBoardAPI
    {
        private const string BOARD_ID = "e6";

        private ILog _log = LogManager.GetLogger(typeof(e6Crawler));

        /// <summary> Webclient for Downloading the Submissions </summary>
        private WebClient _client = new WebClient();

        /// <summary> Connection to the Database </summary>
        private IDBConnector _database;

        /// <summary> Login Data for e621 </summary>
        private string _username;
        private string _key;
        /// <summary> Store for the Login Data for HTTP Basic Auth </summary>
        private System.Net.CredentialCache _credentialCache = new System.Net.CredentialCache();
        /// <summary> URL of the Plattform </summary>
        private string _baseUrl = "https://e621.net/";
        /// <summary> Last Time a Request was made </summary>
        private DateTime _lastRequest = DateTime.Now;

        /// <summary> All Collections to Download </summary>
        private List<IPostCollection> _toSave = new List<IPostCollection>();
        /// <summary> Tags to classify Submissions as Explicit </summary>
        private List<string[]> _extremeTags = new List<string[]>();
        /// <summary> Crawler won't download Submissions with these Tags </summary>
        private List<string[]> _blackListTags = new List<string[]>();

        /// <summary> Where to store the Submissions </summary>
        string IBoardAPI.storageTarget { get; set; }

        public e6Crawler(IDBConnector database)
        {
            _database = database;

            _client.Headers["User-Agent"] = "BoardCrawler by Avis Jay";
        }

        /// <summary> Backup Submissions
        /// </summary>
        public void DownloadFavs()
        {
            //Backup all Collections
            foreach(IPostCollection collection in _toSave)
            {
                collection.Save(this);
            }
        }

        /// <summary>
        /// Read Config
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public bool Login(XmlNode config)
        {
            //Get Login Data
            _username = config["Username"].InnerText;
            _key = config["ApiKey"].InnerText;

            //Get Collections to Backup
            var toBackup = config["ToBackup"];

            //Backup Users own Favourites
            string parsed = toBackup["BackupOwnFavs"]?.InnerText ?? "False";
            if (bool.Parse(parsed))
                _toSave.Add(new FavToSave(_username));

            //Add all Collections
            foreach(var children in toBackup.ChildNodes)
            {
                XmlNode childNode = children as XmlNode;
                switch(childNode.LocalName)
                {
                    case "BackupFav":
                        _toSave.Add(new FavToSave(childNode.InnerText));
                        break;

                    case "BackupPool":
                        _toSave.Add(new PoolToSave(childNode.InnerText));
                        break;

                    case "BackupPoolSet":
                        _toSave.Add(new PoolSetToSave(childNode.InnerText));
                        break;

                    case "BackupSet":
                        _toSave.Add(new SetToSave(childNode.InnerText));
                        break;
                }
            }

            //Read Tags for Tagging Submissions as Extreme
            if(config["ExtremeTags"] != null)
            {
                foreach(var tag in config["ExtremeTags"].ChildNodes)
                {
                    XmlNode childNode = tag as XmlNode;
                    _extremeTags.Add(childNode.InnerText.Split(','));
                }
            }

            //Posts with these Tags must not be downloaded
            if(config["BlackList"] != null)
            {
                foreach(var tag in config["BlackList"].ChildNodes)
                {
                    XmlNode childNode = tag as XmlNode;
                    _blackListTags.Add(childNode.InnerText.Split(','));
                }
            }
            //Add Credentials to the Cache
            _credentialCache.Add(new System.Uri(_baseUrl), "Basic", new System.Net.NetworkCredential(_username, _key,_baseUrl));

            //Sadly I haven't found a way to check, if the credentials are Valid yet
            return true;
        }

        /// <summary>
        /// Logout not Needed
        /// </summary>
        /// <returns></returns>
        public bool Logout()
        {
            return true;
        }

        /// <summary>
        /// Send a single Request to the e621 Api.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        private bool sendRequest(string url, out string response)
        {
            //We must only send one Submission per Second
            if (DateTime.Now - _lastRequest < new TimeSpan(0, 0, 1))
                Thread.Sleep(new TimeSpan(0, 0, 1));

            //Send Request to the Server
            return IBoardAPI.SendRequest(url, out response, 5, _credentialCache);
        }

        /// <summary>
        /// Download a Single Post from the e621 Database
        /// </summary>
        /// <param name="json"></param>
        /// <param name="additionalTags"></param>
        private long ReadPost(JsonElement json, string[] additionalTags, long collection = -1, long cindex = -1)
        {
            _log.Info("Read Submission");

            //Read Metadata for the Post
            long id;
            List<string> keywords = new List<string>();

            DateTime created = DateTime.Now;
            DateTime updated = DateTime.Now;
            Rating rating = Rating.Safe;
            string description;

            //Read basic Information
            id = json.GetProperty("id").GetInt64();

            _log.Info("Id:" + id);

            created = json.GetProperty("created_at").GetDateTime();
            updated = json.GetProperty("updated_at").GetDateTime();

            //Read Path of Image on the Server
            string path = json.GetProperty("file").GetProperty("url").GetString();

            //Build Path to Store the Image
            string fileType = path.Split('.').Last();
            string storagePath = ((IBoardAPI)this).getStoragePath(BOARD_ID, id); //todo: Sub Ordner erstellen für Metadata

            //Map Rating
            switch (json.GetProperty("rating").GetString())
            {
                case "s":
                    rating = Rating.Safe;
                    break;

                case "e":
                    rating = Rating.Explicit;
                    break;

                case "q":
                    rating = Rating.Questionable;
                    break;
            }

            //Get Tags
            keywords = getKeywords(json.GetProperty("tags"));
            //Add Additional Tags
            keywords.AddRange(additionalTags);

            string[] artists = getArtists(json.GetProperty("tags"));

            //If the Tags contain the Tag "comic"
            SubmissionType subType = SubmissionType.Image;
            if (keywords.Contains("comic"))
                subType = SubmissionType.Comic;//The Submission is a Comic Page

            //Read Image Description
            description = json.GetProperty("description").GetString();

            //If the Tags contain an Tag from the Extreme list
            if (_extremeTags.Any(t => t.All(e => keywords.Contains(e))))
            {
                //Adjust Rating
                rating = Rating.Extreme;
            }
            //IF the Tags do not Contain a Blacklisted Tag
            if(!_blackListTags.Any(t => t.All(e => keywords.Contains(e))))
            {
                long nid;
                //Try to Insert the Submissions Data into the Database
                if (_database.InsertSubmission(id, BOARD_ID, subType, "e6 " + id.ToString(), updated, created, storagePath, rating, new int[artists.Length], artists, description, keywords.ToArray(),out nid))
                {
                    //Was the Post Updated, download the new Image
                    string localPath = Path.Combine(storagePath, id + "." + fileType);
                    _log.Info("Save to " + localPath);
                    _client.DownloadFile(path, localPath);
                }

                if(collection > -1)
                    _database.AddToCollection(nid, collection, cindex);
                return nid;
            }
            else
            {
                _log.Info("Download of " + id + " was blocked because of Blacklisted Keywords " + string.Join(',', _blackListTags.Where(t => t.All(e => keywords.Contains(e)))));
                return -1;
            }
        }

        /// <summary>
        /// Read Keywords from a JSON from the Api
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        private List<string> getKeywords(JsonElement json)
        {
            List<string> tags = new List<string>();

            //Enumerate all Tag Categories
            foreach(var category in json.EnumerateObject())
            {
                //Enumerate all Tags in the Categories
                foreach(var tagsInCat in category.Value.EnumerateArray())
                {
                    //Add Tag
                    tags.Add(tagsInCat.GetString());
                }
            }
            return tags;
        }

        private string[] getArtists(JsonElement json)
        {
            List<string> tags = new List<string>();

            string txt = json.ToString();

            var artists = json.GetProperty("artist");
            foreach(var singleArtist in artists.EnumerateArray())
            {
                tags.Add(singleArtist.GetString());
            }

            return tags.ToArray();
        }

        #region Collections
        interface IPostCollection
        {
            /// <summary>
            /// Save the Collection
            /// </summary>
            /// <param name="crawler"></param>
            public void Save(e6Crawler crawler);
        }

        /// <summary>
        /// A Post Collection to Save. Abstract base
        /// </summary>
        abstract class CollectionToSave : IPostCollection
        {
            private ILog _log = LogManager.GetLogger(typeof(CollectionToSave));
            protected long _cid;
            
            private string _collectionName;
            private string _collectionType;
            private string _collectionDescription;

            public CollectionToSave(string collectionType)
            {
                _collectionType = collectionType;
            }

            protected virtual string getURL(e6Crawler crawler)
            {
                return string.Empty;
            }

            protected virtual void InsertCollection(e6Crawler crawler, string name, string description)
            {
                _cid = crawler._database.InsertCollection(e6Crawler.BOARD_ID, _collectionType, name, description);
            }

            /// <summary>
            /// Save the Collection
            /// </summary>
            /// <param name="crawler"></param>
            public void Save(e6Crawler crawler)
            {
                _log.Info("Save " + Name);

                int page = 1;

                string jsonResp;
                bool reachedEnd = false;

                InsertCollection(crawler, _collectionName, _collectionDescription);

                //Loop over all Pages
                while (!reachedEnd)
                {
                    //Get the Page of the Collection
                    string requestUrl = getURL(crawler) + "&page=" + page + "&login=" + crawler._username + "&api_key=" + crawler._key;
                    //Request Page of the Collection
                    if (crawler.sendRequest(requestUrl, out jsonResp))
                    {
                        //If the Server returns an Empty Collection there are no more Pages
                        if (jsonResp == "{\"posts\":[]}")
                        {
                            //Have we reached the last Page
                            reachedEnd = true;
                            _log.Info("Reached end of List");
                        }
                        //The URL doesn't point to a Valid Page
                        else if (jsonResp == "{ \"success\":false,\"reason\":\"not found\"}")
                        {
                            reachedEnd = true;
                            _log.Warn("Couldn't find List" + requestUrl);
                        }
                        //The Server has Returned a Valid response
                        else
                        {
                            try
                            {
                                //Parse the Response
                                JsonDocument response = JsonDocument.Parse(jsonResp);
                                //Iterate over all Posts in the Collection
                                IteratePosts(response.RootElement,crawler);
                            }
                            catch (Exception ex)
                            {
                                _log.Error("Error on Parsing Page. " + ex.Message + ex.StackTrace);
                            }
                        }

                        //Try next Page
                        page++;
                    }
                    else
                    {
                        _log.Warn("Reached end of List");
                    }
                }
            }

            /// <summary>
            /// Read all Posts in the Collection
            /// </summary>
            /// <param name="root"></param>
            /// <param name="crawler"></param>
            protected virtual void IteratePosts(JsonElement root, e6Crawler crawler)
            {
                var posts = root.GetProperty("posts");
                long position = 0;
                foreach (var post in posts.EnumerateArray())
                {                    
                    _log.Info("Read Post");
                    crawler.ReadPost(post, new string[0], _cid, position);
                    position++;
                }
            }

            /// <summary>
            /// Name for Printing
            /// </summary>
            protected virtual string Name
            {
                get
                {
                    return "Collection";
                }
            }
        }

        /// <summary>
        /// A Pool to Save
        /// </summary>
        class PoolToSave : CollectionToSave
        {
            private ILog _log = LogManager.GetLogger(typeof(FavToSave));
            private string _id;
            
            public PoolToSave(string id):base("Pool")
            {
                _id = id;
            }

            protected override void InsertCollection(e6Crawler crawler, string name, string description)
            {
                string resp = "";
                if(crawler.sendRequest("https://e621.net/pools/" + _id + ".json",out resp))
                {
                    JsonDocument poolData = JsonDocument.Parse(resp);
                    name = poolData.RootElement.GetProperty("name").GetString();
                    description = poolData.RootElement.GetProperty("description").GetString();
                }

                base.InsertCollection(crawler, name, description);
            }

            protected override string getURL(e6Crawler crawler)
            {
                return crawler._baseUrl + "/posts.json?tags=pool%3A" + _id;
            }

            protected override void IteratePosts(JsonElement root, e6Crawler crawler)
            {
                _log.Info("Read Pool " + _id);
                long index = 0;
                foreach (var post in root.GetProperty("posts").EnumerateArray())
                {
                    crawler.ReadPost(post, new string[0], _cid, index);
                    index++;
                }
            }

            protected override string Name => "Pool:" + _id;
        }

        /// <summary>
        /// A Set containing Pages from Pools to Save
        /// </summary>
        class PoolSetToSave : CollectionToSave
        {
            string _id;

            public PoolSetToSave(string id) : base("Pool Set")
            {
                _id = id;
            }

            protected override string getURL(e6Crawler crawler)
            {
                return crawler._baseUrl + "/posts.json?tags=set%3A" + _id;
            }

            /// <summary>
            /// Iterate over all Images in the Set
            /// </summary>
            /// <param name="root"></param>
            /// <param name="crawler"></param>
            protected override void IteratePosts(JsonElement root, e6Crawler crawler)
            {
                var posts = root.GetProperty("posts");
                foreach (var post in posts.EnumerateArray())
                {
                    //Save all Pools the Submission is part of
                    var pools = post.GetProperty("pools");
                    foreach(var pool in pools.EnumerateArray())
                    {
                        PoolToSave toSave = new PoolToSave(pool.GetInt64().ToString());             
                        toSave.Save(crawler);
                    }
                }
            }
            protected override string Name => "Pool Set:" + _id;
        }

        /// <summary>
        /// A Set to Backup
        /// </summary>
        class SetToSave : CollectionToSave
        {
            private ILog _log = LogManager.GetLogger(typeof(FavToSave));
            private string _id;

            public SetToSave(string id):base("Set")
            {
                _id = id;
            }

            protected override string getURL(e6Crawler crawler)
            {
                return crawler._baseUrl + "/posts.json?tags=set%3A" + _id;
            }

            protected override void IteratePosts(JsonElement root, e6Crawler crawler)
            {
                long index = 0;
                try
                {
                    foreach (var post in root.GetProperty("posts").EnumerateArray())
                    {
                        crawler.ReadPost(post, new string[0], _cid, index);
                        index++;
                    }
                }
                catch(Exception ex)
                {
                    _log.Error(ex.Message + ex.StackTrace);
                }
            }
            protected override string Name => "Set:" + _id;
        }

        /// <summary>
        /// Favs of a User to Save
        /// </summary>
        class FavToSave : CollectionToSave
        {
            private ILog _log = LogManager.GetLogger(typeof(FavToSave));
            private string _userName;

            public FavToSave(string userName):base("Fav")
            {
                _userName = userName;
            }

            protected override string getURL(e6Crawler crawler)
            {
                return crawler._baseUrl + "/posts.json?tags=fav%3A" + crawler._username;
            }
            protected override string Name => "Favourites of " + _userName;
        }
        #endregion
    }
}
