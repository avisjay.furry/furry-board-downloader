﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using log4net;

namespace BoardCrawlerLib
{
    public class JSONStorage : IDBConnector
    {
        private static ILog _log = LogManager.GetLogger(typeof(JSONStorage));

        List<CollectionDetails> _collections = new List<CollectionDetails>();
        List<Submitter> _allSubmitters = new List<Submitter>();
        List<SubmissionInCollection> _subsInColls = new List<SubmissionInCollection>();
        Dictionary<string, List<long>> _keywordCache = new Dictionary<string, List<long>>();
        Dictionary<long, string> _fileCache = new Dictionary<long, string>();

        GlobalData _gdata = new GlobalData() { MaxId = 0 };

        private string _path;

        public JSONStorage(string path)
        {
            _path = path;
            if (!Directory.Exists(_path))
                Directory.CreateDirectory(_path);
            else
            {
                CountDownLock finished = new CountDownLock(5);

                Task.Run(() =>
                {
                    try
                    {
                        _log.Info("Load Collection Data");
                        string colsPath = Path.Combine(_path, "JSONData", "Collections.json");
                        if (File.Exists(colsPath))
                        {
                            string subs = File.ReadAllText(colsPath);
                            _collections.AddRange(JsonSerializer.Deserialize<List<CollectionDetails>>(subs));
                        }
                        _log.Info("Finished Loading Collection Data");
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex.Message + ex.StackTrace);
                    }
                    finally
                    {
                        finished.CountDown();
                    }
                });

                Task.Run(() =>
                {
                    try
                    {
                        _log.Info("Load Submitter Data");
                        string subsPath = Path.Combine(_path, "JSONData", "Submitters.json");
                        if (File.Exists(subsPath))
                        {
                            string subs = File.ReadAllText(subsPath);
                            _allSubmitters.AddRange(JsonSerializer.Deserialize<List<Submitter>>(subs));
                        }
                        _log.Info("Finished Loading Submitter Data");
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex.Message + ex.StackTrace);
                    }
                    finally
                    {
                        finished.CountDown();
                    }
                });

                Task.Run(() =>
                {
                    try
                    {
                        _log.Info("Load Submission Collection Data");
                        string subsPath = Path.Combine(_path, "JSONData", "SubmissionsInCollections.json");
                        if (File.Exists(subsPath))
                        {
                            string subs = File.ReadAllText(subsPath);
                            _subsInColls.AddRange(JsonSerializer.Deserialize<List<SubmissionInCollection>>(subs));
                        }
                        _log.Info("Finished Loading Submission Collection Data");
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex.Message + ex.StackTrace);
                    }
                    finally
                    {
                        finished.CountDown();
                    }
                });

                Task.Run(() =>
                {
                    try
                    {
                        _log.Info("Load Keyword Cache");
                        string keysPath = Path.Combine(_path, "JSONData", "KeywordCache.json");
                        if (File.Exists(keysPath))
                        {
                            string keyw = File.ReadAllText(keysPath);
                            _keywordCache = JsonSerializer.Deserialize<Dictionary<string, List<long>>>(keyw);
                        }
                        _log.Info("Finished Loading Keyword Cache");
                        finished.CountDown();
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex.Message + ex.StackTrace);
                    }
                    finally
                    {
                        finished.CountDown();
                    }
                });

                Task.Run(() =>
                {
                    try
                    {
                        _log.Info("Load Global Data");
                        string keysPath = Path.Combine(_path, "JSONData", "global.json");
                        if (File.Exists(keysPath))
                        {
                            string keyw = File.ReadAllText(keysPath);
                            _gdata = JsonSerializer.Deserialize<GlobalData>(keyw);
                        }
                        _log.Info("Finished Loading Global Data");
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex.Message + ex.StackTrace);
                    }
                    finally
                    {
                        finished.CountDown();
                    }
                });

                finished.Await();
            }
        }

        public bool connected
        {
            get
            {
                return Directory.Exists(_path);
            }
        }

        public void CleanUpKeywords()
        {
            _keywordCache.RemoveAll(r => r.Count == 0);
        }

        public void Close()
        {
            CountDownLock finished = new CountDownLock(5);

            Task.Run(() =>
            {
                _log.Info("Save Collections");
                string cols = JsonSerializer.Serialize(_collections);
                File.WriteAllText(Path.Combine(_path, "JSONData", "Collections.json"), cols);
                _log.Info("Collections saved");
                finished.CountDown();                
            });
            Task.Run(() =>
            {
                _log.Info("Save Submitters");
                string subms = JsonSerializer.Serialize(_allSubmitters);
                File.WriteAllText(Path.Combine(_path, "JSONData", "Submitters.json"), subms);
                _log.Info("Submitters saved");
                finished.CountDown();
            });
            Task.Run(() =>
            {
                _log.Info("Save Submissions in Collections");
                string subInCols = JsonSerializer.Serialize(_subsInColls);
                File.WriteAllText(Path.Combine(_path, "JSONData", "SubmissionsInCollections.json"), subInCols);
                _log.Info("Submissions in Collections saved");
                finished.CountDown();
            });
            Task.Run(() =>
            {
                _log.Info("Save Keyword Cache");
                string keycache = JsonSerializer.Serialize(_keywordCache);
                File.WriteAllText(Path.Combine(_path, "JSONData", "KeywordCache.json"), keycache);
                _log.Info("Keyword Cache saved");
                finished.CountDown();
            });
            Task.Run(() =>
            {
                _log.Info("Save Global Data");
                string gData = JsonSerializer.Serialize(_gdata);
                File.WriteAllText(Path.Combine(_path, "JSONData", "global.json"), gData);
                _log.Info("Global Data saved");
                finished.CountDown();
            });

            finished.Await();
        }

        public CollectionDetails GetCollectionDetails(long collectionId)
        {
            return _collections.Where(c => c.CollectionId == collectionId).FirstOrDefault();
        }

        public long[] getSubmissionsForKeywords(string[] keywords)
        {
            if (keywords.Length == 0 || !_keywordCache.ContainsKey(keywords[0]))
                return new long[0];

            IEnumerable<long> remaining = new List<long>(_keywordCache[keywords[0]]);
            
            foreach(string s in keywords.Skip(1))
            {
                if (!_keywordCache.ContainsKey(s))
                    return new long[0];

                remaining = remaining.Where(r => _keywordCache[s].Contains(r));
            }
            return remaining.ToArray();
        }

        public long GetSubmitter(string plattform, string name)
        {
            return _allSubmitters.Where(s => s.Plattform == plattform && s.UserNames.Any(n => n.name == name)).Select(s => s.SubmitterId).FirstOrDefault();
        }

        public Submitter GetSubmitterDetails(long submitterId)
        {
            return _allSubmitters.Where(s => s.SubmitterId == submitterId).FirstOrDefault();
        }

        public long InsertCollection(string plattform, string type, string name, string description)
        {
            var col = _collections.Where(c => c.Plattform == plattform && c.Name == name && c.Type == type);

            if (!col.Any())
            {
                long newId = _collections.MaxOrDefault(f => f.CollectionId,-1) + 1;
                _collections.Add(new CollectionDetails(newId, name, plattform, type, description));
                return newId;
            }
            else
            {
                return col.First().CollectionId;
            }
        }

        public bool InsertSubmission(long subid, string plattform, SubmissionType type, string name, DateTime updated, DateTime created, string path, Rating rating, int[] submitterId, string[] submitterName, string description, string[] keywords, out long id)
        {
            long[] submIds = updateUserNames(plattform, submitterId, submitterName);

            bool save = true;

            string metaFile = path + "//meta.json";

            SubmissionDetails newDetails = null;
            SubmissionDetails oldDetails = null;
            long? iid = null;

            if (Directory.Exists(path))
            {
                if (File.Exists(metaFile))
                {
                    string metaData = File.ReadAllText(metaFile);
                    oldDetails = JsonSerializer.Deserialize<SubmissionDetails>(metaData);
                    if (oldDetails.Updated >= updated)
                        save = false;

                    iid = oldDetails.FileId;
                }
            }
            else
            {
                Directory.CreateDirectory(path);
            }

            if (iid == null)
            {
                iid = getMaxSubId();

                _fileCache.Add(iid.Value, path);
            }

            newDetails = new SubmissionDetails(iid.Value, plattform, subid, created, updated, type, rating, keywords, path, _allSubmitters.Where(s => submIds.Contains(s.SubmitterId)).ToArray());
            if (newDetails != oldDetails)
            {
                string metaToSave = JsonSerializer.Serialize(newDetails);
                File.WriteAllText(metaFile, metaToSave);

                IEnumerable<string> toAdd = new List<string>();
                if(oldDetails != null)
                    toAdd = newDetails.Keywords.Except(oldDetails.Keywords);

                foreach(string s in toAdd)
                {
                    if(_keywordCache.ContainsKey(s))
                    {
                        _keywordCache[s].Add(iid.Value);
                    }
                    else
                    {
                        _keywordCache.Add(s, new List<long>() { iid.Value });
                    }
                }

                if (oldDetails != null)
                {
                    var toRemove = oldDetails.Keywords.Except(newDetails.Keywords);
                    foreach (string s in toRemove)
                    {
                        if (_keywordCache.ContainsKey(s))
                        {
                            _keywordCache[s].Remove(iid.Value);
                            if (_keywordCache[s].Count == 0)
                                _keywordCache.Remove(s);
                        }
                    }
                }
                else
                {
                    foreach (string keyw in newDetails.Keywords)
                    {
                        List<long> subs;
                        if (_keywordCache.TryGetValue(keyw, out subs))
                        {
                            subs.Add(iid.Value);
                        }
                        else
                        {
                            _keywordCache.Add(keyw, new List<long>() { iid.Value });
                        }
                    }
                }
            }

            id = iid.Value;
            return save;
        }

        private long getMaxSubId()
        {
            if(_gdata.MaxId == -1)
            {
                _gdata.MaxId = _fileCache.Max(f => f.Key) + 1;
                return _gdata.MaxId;
            }
            else
            {
                _gdata.MaxId++;
                return _gdata.MaxId;
            }
        }

        private long[] updateUserNames(string plattform, int[] submitterId, string[] submitterNames)
        {
            List<long> ids = new List<long>();

            for(int i = 0; i < submitterId.Count(); i++)
            {
                ids.Add(updateUserName(plattform, submitterId[i], submitterNames[i]));
            }
            return ids.ToArray();
        }
        private long updateUserName(string plattform, int submitterId, string submitterName)
        {
            IEnumerable<Submitter> subm;

            if (submitterId == 0)
            {
                subm = _allSubmitters.Where(s => s.Plattform == plattform && s.UserNames.Any(s => s.name == submitterName));
            }
            else
            {
                subm = _allSubmitters.Where(s => s.Plattform == plattform && s.PlattformId == submitterId);
            }
            if (subm.Any())
            {
                var sub = subm.First();
                if (sub.CurrentUsername == submitterName)
                    sub.UserNames.Where(u => u.name == submitterName).First().lastSeen = DateTime.Now;
                else
                    sub.UserNames.Add(new NameDate() { id = sub.UserNames.MaxOrDefault(un => un.id, -1) + 1, lastSeen = DateTime.Now, name = submitterName });

                return sub.SubmitterId;
            }
            else
            {
                long newUserId = _allSubmitters.MaxOrDefault(s => s.SubmitterId, -1) + 1;
                _allSubmitters.Add(new Submitter(newUserId, submitterId, plattform, new NameDate[1]
                {
                    new NameDate
                    {
                        id = 0,
                        lastSeen = DateTime.Now,
                        name = submitterName
                    }
                }));

                return newUserId;
            }

        }

        public void wipeDB()
        {
            if(File.Exists(Path.Combine("Collections.json")))
                File.Delete(Path.Combine(_path, "Collections.json"));

            if (File.Exists(Path.Combine(_path, "Submitters.json")))
                File.Delete(Path.Combine(_path, "Submitters.json"));

            if (File.Exists(Path.Combine(_path, "SubmissionsInCollections.json")))
                File.Delete(Path.Combine(_path, "SubmissionsInCollections.json"));

            if (File.Exists(Path.Combine(_path, "KeywordCache.json")))
                File.Delete(Path.Combine(_path, "KeywordCache.json"));
        }

        public void AddToCollection(long subid, long colid)
        {
            if (!_subsInColls.Any(c => c.SubmissionId == subid && c.CollectionId == colid))
                _subsInColls.Add(new SubmissionInCollection()
                {
                    SubmissionId = subid,
                    CollectionId = colid
                });
        }

        public void RemoveFromCollection(long subid, long colid)
        {
            _subsInColls.RemoveAll(c => c.SubmissionId == subid && c.CollectionId == colid);
        }

        public SubmissionDetails GetSubmissionDetails(long submissionId)
        {
            string filePath;
            if (_fileCache.TryGetValue(submissionId, out filePath))
            {
                if (Directory.Exists(filePath))
                {
                    string metaFile = filePath + "//meta.json";
                    if (File.Exists(metaFile))
                    {
                        string metaData = File.ReadAllText(metaFile);
                        SubmissionDetails details = JsonSerializer.Deserialize<SubmissionDetails>(metaData);
                        return details;
                    }
                }
            }
            return null;
        }

        public void AddToCollection(long subid, long colid, long index)
        {
            var entry = _subsInColls.Where(s => s.SubmissionId == subid && colid == s.CollectionId);
            if (entry.Any())
            {
                entry.First().Position = index;
            }
            else
            {
                _subsInColls.Add(new SubmissionInCollection() { CollectionId = colid, SubmissionId = subid, Position = index });
            }
        }

        public long[] GetFilesInCollection(long collectionId)
        {
            return _subsInColls.Where(s => s.CollectionId == collectionId)
                .OrderBy(s => s.Position).Select(c => c.SubmissionId).ToArray();
        }

        public long[] GetCollectionsFileIsIn(long file)
        {
            return _subsInColls.Where(s => s.SubmissionId == file).Select(s => s.CollectionId).ToArray();
        }
    }

    class GlobalData
    {
        public long MaxId { get; set; }
    }

    class SubmissionInCollection
    {        
        public long SubmissionId { get; set; }
        public long CollectionId { get; set; }
        public long Position { get; set; }
    }
}
