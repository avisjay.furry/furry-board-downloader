﻿using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace BoardCrawlerLib
{
    /// <summary>
    /// Still in Development
    /// </summary>
    class SFCrawler : IBoardAPI
    {
        private ILog _log = LogManager.GetLogger(typeof(SFCrawler));
        private Authentification _auth;

        //Doku: https://wiki.sofurry.com/wiki/SoFurry_2.0_API
        //https://wiki.sofurry.com/wiki/How_to_use_OTP_authentication

        private IDBConnector _db;
        private RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();
        int otp_sequence = 0;
        string otp_pad;
        string otp_salt;

        string IBoardAPI.storageTarget { get; set; }

        public SFCrawler(IDBConnector db)
        {
            this._db = db;
        }

        public void DownloadFavs()
        {
            throw new NotImplementedException();
        }

        public bool Login(XmlNode creds)
        {            
            otp_pad = rngString(35);
            otp_salt = rngString(15);

            //_auth.username = c[0];
            //_auth.password = c[1];

            _auth.setCurrentAuthentificationSalt(otp_salt);

            return false;
        }
        private string rngString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[getRndint(chars.Length)]).ToArray());
        }
        private int getRndint(int max)
        {
            byte[] b = new byte[4];
            _rng.GetBytes(b);
            int i = BitConverter.ToInt32(b, 0);
            return i % max;
        }

        public bool Logout()
        {
            throw new NotImplementedException();
        }

        public void SetStorageTarget(string storagePath)
        {
            throw new NotImplementedException();
        }

        void IBoardAPI.DownloadFavs()
        {
            throw new NotImplementedException();
        }

        bool IBoardAPI.Logout()
        {
            throw new NotImplementedException();
        }
    }

    class Authentification
    {
        private ILog _log = LogManager.GetLogger(typeof(Authentification));

        private string authentificationPadding = "@6F393fk6FzVz9aM63CfpsWE0J1Z7flEl9662X";
        private long currentAuthentificationSequence = 0;
        private string salt = "";

        public const int AJAXTYPE_APIERROR = 5;
        public const int AJAXTYPE_OTPAUTH = 6;

        public string username;
        public string password;

        public Authentification(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        public static string getMd5Hash(string input)
        {
            using (MD5 hasher = MD5.Create())
            {
                byte[] hash = hasher.ComputeHash(System.Text.UTF8Encoding.UTF8.GetBytes(input));
                long number = BitConverter.ToInt64(hash, 0);
                string md5 = Convert.ToString(number, 16);
                while (md5.Length < 32)
                    md5 = 0 + md5;
                return md5;
            }
        }
        public string generateRequestHash()
        {
            string hashedPassword = getMd5Hash(password + salt);
            string hash = getMd5Hash(hashedPassword + authentificationPadding + currentAuthentificationSequence);
            return hash;
        }
        public Dictionary<string,string> addAuthParameterToQuery(Dictionary<string, string> queryParams) 
        {
            queryParams.Add("otpuser", username);
            queryParams.Add("otphash", generateRequestHash());
            queryParams.Add("optsquence", currentAuthentificationSequence.ToString());
            currentAuthentificationSequence++;
            return queryParams;
        }
        public void setCurrentAuthentificationSalt(string newSalt)
        {
            this.salt = newSalt;
        }
        public string getUsername()
        {
            return this.salt;
        }
        public string getPassword()
        {
            return this.password;
        }

        public bool parseResponse(string httpResult)
        {
            try
            {
                JObject json = JObject.Parse(httpResult);
                int messageType = json.Value<int>("messageType");
                if(messageType == AJAXTYPE_OTPAUTH)
                {
                    currentAuthentificationSequence = json.Value<int>("newSequence");
                    authentificationPadding = json.Value<string>("newPadding");
                    salt = json.Value<string>("newSalt");
                    string otpVersion = json.Value<string>("version");
                    return false;
                }
            }
            catch(Exception ex)
            {
                _log.Error(ex.Message + ex.StackTrace);
            }
            return true;
        }
    }
}
