﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace BoardCrawlerLib
{
    static class LinqExtension
    {
        public static int MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
        {
            return source.MaxOrDefault(selector, 0);
        }
        public static int MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector, int def)
        {
            if (!source.Any())
                return def;
            return source.Max(selector);
        }

        public static long MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
        {
            return source.MaxOrDefault(selector, 0);
        }
        public static long MaxOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector, long def)
        {
            if (!source.Any())
                return def;
            return source.Max(selector);
        }
        public static int RemoveAll<TKey,TData>(this Dictionary<TKey,TData> dict, Func<TData, bool> selector)
        {
            List<TKey> toRemove = new List<TKey>();
            foreach(KeyValuePair<TKey,TData> contents in dict)
            {
                if (selector.Invoke(contents.Value))
                    toRemove.Add(contents.Key);
            }
            foreach(TKey rem in toRemove)
            {
                dict.Remove(rem);
            }
            return toRemove.Count;
        }
    }
}
