﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;

namespace BoardCrawlerLib
{
    /// <summary>
    /// Connection to a Database
    /// </summary>
    public interface IDBConnector
    {
        public bool connected { get; }
        public void wipeDB();
        public bool InsertSubmission(long subid, string plattform, SubmissionType type, string name, DateTime updated, DateTime created, string path, Rating rating, int[] submitterId, string[] submitterName, string description, string[] keywords, out long id);
        public long InsertCollection(string plattform, string type, string name, string description);
        public void AddToCollection(long subid, long colid, long index);
        public void RemoveFromCollection(long subid, long colid);
        public void CleanUpKeywords();
        public long[] getSubmissionsForKeywords(string[] keywords);
        public long GetSubmitter(string plattform, string name);
        public Submitter GetSubmitterDetails(long submitterId);
        public CollectionDetails GetCollectionDetails(long collectionId);
        public SubmissionDetails GetSubmissionDetails(long submissionId);
        public long[] GetFilesInCollection(long collectionId);
        public void Close();
    }

    public class SubmissionDetails
    {
        public long FileId { get; set; }
        public string Plattform { get; set; }
        public long SubmissionId { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public SubmissionType Type { get; set; }
        public Rating SubmissionRating { get; set; }
        public string[] Keywords { get; set; }
        public string Path { get; set; }
        [JsonIgnore]
        public Submitter[] Submitter { get; set; }
        public long[] SubmitterId { get; set; }

        public SubmissionDetails()
        {
        }

        public SubmissionDetails(long fileId, string plattform, long submissionId, DateTime created, DateTime updated, SubmissionType type, Rating submissionRating, string[] keywords, string path, Submitter[] submitter)
        {
            this.FileId = fileId;
            this.Plattform = plattform;
            this.SubmissionId = submissionId;
            this.Created = created;
            this.Updated = updated;
            this.Type = type;
            this.SubmissionRating = submissionRating;
            this.Keywords = keywords;
            this.Path = path;
            this.Submitter = submitter;
            this.SubmitterId = submitter.Select(s => s.SubmitterId).ToArray();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj is SubmissionDetails)
            {
                SubmissionDetails compareTo = obj as SubmissionDetails;
                if (compareTo.FileId != FileId)
                    return false;
                if (compareTo.Plattform != Plattform)
                    return false;
                if (compareTo.SubmissionId != SubmissionId)
                    return false;
                if (compareTo.Name != Name)
                    return false;
                if (compareTo.Created != Created)
                    return false;
                if (compareTo.Updated != Updated)
                    return false;
                if (compareTo.Type != Type)
                    return false;
                if (compareTo.SubmissionRating != SubmissionRating)
                    return false;
                if (!compareTo.Keywords.Compare(Keywords))
                    return false;
                if (compareTo.Path != Path)
                    return false;
                if (!compareTo.SubmitterId.Compare(SubmitterId))
                    return false;

                return true;
            }
            else
                return false;
        }

        public static bool operator ==(SubmissionDetails subm1, SubmissionDetails subm2)
        {
            if (((object)subm1) == null)
            {
                if (((object)subm2) == null)
                    return true;
                return false;
            }

            return subm1.Equals(subm2);
        }
        public static bool operator !=(SubmissionDetails subm1, SubmissionDetails subm2)
        {
            if (subm1 == null)
            {
                if (subm2 == null)
                    return false;
                return true;
            }

            return !subm1.Equals(subm2);
        }
    }
    public class CollectionDetails
    {
        public long CollectionId { get; set; }
        public string Name { get; set; }
        public string Plattform { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public CollectionDetails()
        {
        }
        public CollectionDetails(long id, string name, string plattform, string type, string description)
        {
            this.CollectionId = id;
            this.Name = name;
            this.Plattform = plattform;
            this.Type = type;
            this.Description = description;
        }
    }
    public class Submitter
    {
        public long SubmitterId { get; set; }
        public long PlattformId { get; set; }
        public string Plattform { get; set; }
        public List<NameDate> UserNames { get; set; }

        public string CurrentUsername
        {
            get
            {
                return UserNames.OrderByDescending(u => u.lastSeen).First().name;
            }
        }

        public Submitter()
        {
        }

        public Submitter(long id, long plattformId, string plattform, NameDate[] names)
        {
            this.SubmitterId = id;
            this.PlattformId = plattformId;
            this.Plattform = plattform;
            this.UserNames = new List<NameDate>(names);
        }
    }
    public class NameDate
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime lastSeen { get; set; }
    }
}
