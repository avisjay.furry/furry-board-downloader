﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using System.Linq;
using log4net;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BoardCrawlerLib
{
    /// <summary>
    /// Connection to a SQL Database
    /// </summary>
    public class SQLDBConnector:IDisposable,IDBConnector
    {
        private ILog _log = LogManager.GetLogger(typeof(SQLDBConnector));

        private MySqlConnection _connection = new MySqlConnection();
        //All SQL Commands
        private MySqlCommand _insertFile;
        private MySqlCommand _getFileBySubId;
        private MySqlCommand _getFileUpdated;
        private MySqlCommand _getKeywordName;
        private MySqlCommand _getKeywordId;
        private MySqlCommand _getKeywordsForFileText;
        private MySqlCommand _getKeywordsForFileId;
        private MySqlCommand _updateFile;
        private MySqlCommand _fileAddKeyword;
        private MySqlCommand _fileRemoveKeyword;
        private MySqlCommand _cleanUpKeywords;
        private MySqlCommand _insertKeyword;
        private MySqlCommand _getUserIds;
        private MySqlCommand _insertUser;
        private MySqlCommand _getUserUsernames;
        private MySqlCommand _addUserUsername;
        private MySqlCommand _updateUsernameLastSeen;
        private MySqlCommand _getFilesWithKeywords;
        private MySqlCommand _getSubDetails;

        private MySqlCommand _getCollection;
        private MySqlCommand _insertCollection;
        private MySqlCommand _updateCollection;

        private MySqlCommand _getCollectionType;
        private MySqlCommand _addCollectionType;

        /// <summary>
        /// Are we Connected to a SQL Database
        /// </summary>
        public bool connected
        {
            get
            {
                return _connection.State == ConnectionState.Open;
            }
        }

        /// <summary>
        /// Wipe the whole Database
        /// </summary>
        public void wipeDB()
        {
            var ticket = _connection.BeginTransaction();
            try
            {
                MySqlCommand _wipeFileKeywords = new MySqlCommand("DELETE FROM files_keywords");
                MySqlCommand _wipeKeywords = new MySqlCommand("DELETE FROM keywords");
                MySqlCommand _wipeUsernames = new MySqlCommand("DELETE FROM usernames");
                MySqlCommand _wipeFiles = new MySqlCommand("DELETE FROM files");
                MySqlCommand _wipeUser = new MySqlCommand("DELETE FROM user");

                _wipeFileKeywords.ExecuteNonQuery();
                _wipeKeywords.ExecuteNonQuery();
                _wipeUsernames.ExecuteNonQuery();
                _wipeFiles.ExecuteNonQuery();
                _wipeUser.ExecuteNonQuery();
                ticket.Commit();
            }
            catch(Exception e)
            {
                ticket.Rollback();
            }
        }

        /// <summary>
        /// Connect to the Database and initialize the SQL Commands
        /// </summary>
        /// <param name="server"></param>
        /// <param name="database"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public void Connect(string server, string database, string username, string password)
        {
            StringBuilder connString = new StringBuilder();
            connString.Append("Server=" + server + ";");
            connString.Append("Database=" + database + ";");
            connString.Append("Uid=" + username + ";");
            connString.Append("Pwd=" + password + ";");

            //Connec to SQL Database
            string builtString = connString.ToString();
            Connect(builtString);
        }
        public void Connect(string connectionString)
        {
            try
            {
                _connection = new MySqlConnection(connectionString);
                //Try to open the connection
                _connection.Open();

                _insertFile = new MySqlCommand("INSERT INTO files (subid,plattform,subtype,name,created,updated,path,rating,description) VALUES (@subid,@plattform,@type,@name,@created,@updated,@path,@rating,@description)", _connection);
                _getFileBySubId = new MySqlCommand("SELECT id FROM files WHERE subid=@subid AND plattform=@plattform", _connection);
                _getFileUpdated = new MySqlCommand("SELECT updated FROM files WHERE id=@id", _connection);
                _getKeywordName = new MySqlCommand("SELECT keyword FROM keywords WHERE id=@id", _connection);
                _getKeywordId = new MySqlCommand("SELECT kwid FROM keywords WHERE keyword=@keyword", _connection);
                _getKeywordsForFileText = new MySqlCommand("SELECT keywords.keyword FROM keywords JOIN file_keywords ON file_keywords.keyword_id = keywords.kwid where file_keywords.file_id=@fileId", _connection);
                _getKeywordsForFileId = new MySqlCommand("SELECT keywords.kwid FROM keywords JOIN file_keywords ON file_keywords.keyword_id = keywords.kwid where file_keywords.file_id=@fileId", _connection);
                _updateFile = new MySqlCommand("UPDATE files SET name=@name, updated=@updated, rating=@rating, description=@description WHERE subid=@subid", _connection);
                _fileAddKeyword = new MySqlCommand("INSERT INTO file_keywords VALUES (@fileId,@keywordId)", _connection);
                _fileRemoveKeyword = new MySqlCommand("DELETE FROM file_keywords WHERE fileId=@fileId AND keywordId=@keywordId", _connection);
                _cleanUpKeywords = new MySqlCommand("DELETE FROM keywords WHERE keyword_id not in (SELECT keyword_id FROM file_keywords)", _connection);
                _insertKeyword = new MySqlCommand("INSERT INTO keywords (keyword) VALUES (@keyword)", _connection);
                _getUserIds = new MySqlCommand("SELECT id FROM user WHERE user_id in (@userids) AND plattform=@plattform", _connection);
                _insertUser = new MySqlCommand("INSERT INTO user (user_id,plattform) VALUES (@userid,@plattform)", _connection);
                _getUserUsernames = new MySqlCommand("SELECT username_id,name,last_seen FROM usernames WHERE user_id = @user_id", _connection);
                _addUserUsername = new MySqlCommand("INSERT INTO usernames (user_id,name,added,last_seen) VALUES (@user_id,@username,@added,@last_seen)", _connection);
                _updateUsernameLastSeen = new MySqlCommand("UPDATE usernames SET last_seen=@last_seen WHERE username_id=@unid", _connection);

                _getFilesWithKeywords = new MySqlCommand("SELECT subid FROM files RIGHT JOIN file_keywords ON files.subid = file_keywords.file_id WHERE file_keyword.keyword_id in (@kws) GROUP BY id HAVING COUNT(*) = @kwsc", _connection);

                _getSubDetails = new MySqlCommand("SELECT plattform,subtype,name,created,updated,path,rating,submitter,description FROM files WHERE subid = @subid", _connection);

                _getCollection = new MySqlCommand("SELECT c.CollectionId, c.Plattform, c.Name, c.Description, t.Name FROM postCollection AS c JOIN CollectionTypes AS t ON c.CollectionType = t.TypeId WHERE c.Plattform = @plattform AND c.Name = @name", _connection);
                _insertCollection = new MySqlCommand("INSERT INTO postCollection (plattform,collectiontype,collectionname,collectiondescription) VALUES (@plattform,@type,@name,@description)", _connection);
                _updateCollection = new MySqlCommand("UPDATE postCollection SET postCollection plattform=@plattform,collectiontype=@type,collectionname=@name,collectiondescription=@description", _connection);

                _getCollectionType = new MySqlCommand("SELECT id FROM collectiontypes WHERE name = @name");
                _addCollectionType = new MySqlCommand("INSERT INTO collectiontypes (Name) VALUES (@name)");
            }
            catch(Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
            }
        }

        /// <summary>
        /// Insert, or Update a Submission to the Database
        /// </summary>
        /// <param name="subid">Id of the Submission</param>
        /// <param name="plattform">Source Plattform of the Submission</param>
        /// <param name="type">Type of the Submission</param>
        /// <param name="name">Name of the Submission</param>
        /// <param name="updated">Last Update Timestamp</param>
        /// <param name="created">Creation Timestamp</param>
        /// <param name="path">Local Storage Path of the File</param>
        /// <param name="rating">Rating of the Post</param>
        /// <param name="submitterId">Plattform Id of the Posts Submitter</param>
        /// <param name="submitterName">Name of the Post Submitter</param>
        /// <param name="description">The Posts Description</param>
        /// <param name="keywords">Keywords of the Post</param>
        /// <returns></returns>
        public bool InsertSubmission(long subid, string plattform, SubmissionType type, string name, DateTime updated, DateTime created, string path, Rating rating, int[] submitterId, string[] submitterName, string description, string[] keywords, out long id)
        {
            try
            {
                var transaction = _connection.BeginTransaction();
                try
                {
                    //Is the submission alread in the database?
                    var saved = GetFileBySubId(subid, plattform, transaction);
                    id = saved;

                    //Try get the Submitter from the Database
                    int[] submitters = getUserIds(submitterId, submitterName, plattform, transaction);
                    //Try updating the Submitters Data
                    updateUsersUsernames(submitterId, submitterName, updated, transaction);

                    //The submission is alread in the database, maybe we have to update it
                    if (saved != -1)
                    {
                        //Get the "Last Updated" for the Submission from our Database
                        _getFileUpdated.Parameters.Clear();
                        _getFileUpdated.Parameters.AddWithValue("id", saved);

                        object datetime = _getFileUpdated.ExecuteScalar();

                        DateTime last_updated = DateTime.Parse(datetime.ToString());
                        //Was it updated since we check
                        if (last_updated < updated)
                        {
                            //Update Data
                            _updateFile.Transaction = transaction;
                            _updateFile.Parameters.Clear();
                            _updateFile.Parameters.AddWithValue("subid", saved);

                            _updateFile.Parameters.AddWithValue("name", name);
                            _updateFile.Parameters.AddWithValue("updated", name);
                            _updateFile.Parameters.AddWithValue("rating", name);
                            _updateFile.Parameters.AddWithValue("description", description);

                            _updateFile.ExecuteNonQuery();

                            //Update Keywords
                            var savedKeywords = getAllKeywordsText(saved);
                            //Add missing Keywords
                            foreach (string kw in keywords)
                            {
                                if (!savedKeywords.Contains(kw))
                                {
                                    //Add Keyword
                                    int keywordId = getKeyword(kw, transaction, true);

                                    _fileAddKeyword.Transaction = transaction;
                                    _fileAddKeyword.Parameters.Clear();
                                    _fileAddKeyword.Parameters.AddWithValue("fileId", saved);
                                    _fileAddKeyword.Parameters.AddWithValue("keywordId", keywordId);

                                    _fileAddKeyword.ExecuteNonQuery();
                                }
                            }
                            //Remove keywords removed
                            foreach (string kw in savedKeywords)
                            {
                                if (!keywords.Contains(kw))
                                {
                                    //Remove Keyword
                                    int keywordId = getKeyword(kw, transaction, false);
                                    _fileRemoveKeyword.Transaction = transaction;
                                    _fileRemoveKeyword.Parameters.Clear();
                                    _fileRemoveKeyword.Parameters.AddWithValue("fileId", saved);
                                    _fileRemoveKeyword.Parameters.AddWithValue("keywordId", keywordId);

                                    _fileRemoveKeyword.ExecuteNonQuery();
                                }
                            }

                            //Execute Transaction
                            transaction.Commit();
                            transaction.Dispose();
                            return true;
                        }
                    }
                    else//The Submission is new
                    {
                        //Create Insertion Command
                        _insertFile.Transaction = transaction;
                        _insertFile.Parameters.Clear();
                        _insertFile.Parameters.AddWithValue("subid", subid);
                        _insertFile.Parameters.AddWithValue("plattform", plattform);
                        _insertFile.Parameters.AddWithValue("type", (int)type);
                        _insertFile.Parameters.AddWithValue("name", name);
                        _insertFile.Parameters.AddWithValue("created", created.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        _insertFile.Parameters.AddWithValue("updated", updated.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        _insertFile.Parameters.AddWithValue("path", path);
                        _insertFile.Parameters.AddWithValue("rating", (int)rating);
                        _insertFile.Parameters.AddWithValue("description", description);

                        updatePostSubmitters(subid, submitters);

                        int inserted = _insertFile.ExecuteNonQuery();

                        //Was the Insertion successfull
                        if (inserted == 1)
                        {
                            //Get Id of the new Submission
                            long newId = GetFileBySubId(subid, plattform, transaction);
                            //Insert Keywords into the Database
                            foreach (string kw in keywords)
                            {
                                //Get Id of the Keyword, create Keyword in the Database if needed.
                                int kid = getKeyword(kw, transaction, true);
                                MySqlCommand fileAddKeyword = (MySqlCommand)_fileAddKeyword;
                                fileAddKeyword.Parameters.Clear();
                                fileAddKeyword.Parameters.AddWithValue("fileId", newId);
                                fileAddKeyword.Parameters.AddWithValue("keywordId", kid);

                                fileAddKeyword.ExecuteNonQuery();
                                fileAddKeyword.Dispose();
                            }
                        }
                        transaction.Commit();
                        transaction.Dispose();
                        return true;
                    }
                    transaction.Commit();
                    transaction.Dispose();
                    return false;
                }
                catch (Exception e)
                {
                    _log.Error(e.Message + e.StackTrace);
                    transaction.Rollback();
                    id = -1;
                    return false;
                }
            }
            catch(Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
                id = -1;
                return false;
            }
        }
        
        public long InsertCollection(string plattform, string type, string name, string description)
        {
            MySqlTransaction transaction = null;

            try
            {
                transaction = _connection.BeginTransaction();
                
                _getCollection.Parameters.Clear();
                _getCollection.Parameters.Add(new MySqlParameter("@plattform", plattform));
                _getCollection.Parameters.Add(new MySqlParameter("@name", name));

                var result = _getCollection.ExecuteReader();
                if(result != null)
                {
                    long lid = result.GetInt64(0);
                    return lid;
                }
                else
                {
                    int ctype = getCollectionType(type);

                    _insertCollection.Parameters.Clear();
                    _insertCollection.Parameters.Add(new MySqlParameter("@plattform", plattform));
                    _insertCollection.Parameters.Add(new MySqlParameter("@name", name));
                    _insertCollection.Parameters.Add(new MySqlParameter("@type", ctype));
                    _insertCollection.Parameters.Add(new MySqlParameter("@description", ctype));

                    _insertCollection.ExecuteNonQuery();

                    _getCollection.Parameters.Clear();
                    _getCollection.Parameters.Add(new MySqlParameter("@plattform", plattform));
                    _getCollection.Parameters.Add(new MySqlParameter("@name", name));

                    var newId = _getCollection.ExecuteReader();

                    return newId.GetInt64(0);
                }
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                return -1;
            }
        }
        private int getCollectionType(string name)
        {
            try
            {
                _getCollectionType.Parameters.Clear();
                _getCollectionType.Parameters.Add(new MySqlParameter("@name", name));

                int? result = (int)_getCollectionType.ExecuteScalar();
                if(result.HasValue)
                {
                    return result.Value;
                }
                else
                {
                    _addCollectionType.Parameters.Clear();
                    _addCollectionType.Parameters.Add(new MySqlParameter("@name", name));
                    _addCollectionType.ExecuteNonQuery();

                    return getCollectionType(name);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Remove Keywords that belong to no Submission
        /// </summary>
        public void CleanUpKeywords()
        {
            try
            {
                _cleanUpKeywords.ExecuteNonQuery();
            }
            catch(Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
            }
        }
        /// <summary>
        /// Get all Keyword for File as Text
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns>Array of the Keywords</returns>
        private string[] getAllKeywordsText(long fileId)
        {
            try
            {
                List<string> allKeywords = new List<string>();

                _getKeywordsForFileText.Parameters.Clear();
                _getKeywordsForFileText.Parameters.AddWithValue("fileId", fileId);
                //Read all Keywords for a Submission as Text
                var results = _getKeywordsForFileText.ExecuteReader();
                //Read all Results
                while (results.Read())
                {
                    allKeywords.Add(results.GetString(0));
                }
                results.Close();
                results.Dispose();

                return allKeywords.ToArray();
            }
            catch(Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
                return new string[0];
            }
        }
        /// <summary>
        /// Get all Keyword Ids for a Post Id
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        private int[] getAllKeywordsId(long fileId)
        {
            try
            {
                List<int> allKeywords = new List<int>();

                _getKeywordsForFileId.Parameters.AddWithValue("fileId", fileId);

                //Read All Key Keyword Ids for a Submission
                var results = _getKeywordsForFileId.ExecuteReader();
                //Read all Results
                while (results.Read())
                {
                    allKeywords.Add(results.GetInt32(0));
                }
                results.Close();
                results.Dispose();

                return allKeywords.ToArray();
            }
            catch(Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
                return new int[0];
            }
        }
        /// <summary>
        /// Get local Id by Submission Id and Plattform
        /// </summary>
        /// <param name="subid"></param>
        /// <param name="plattform"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        private long GetFileBySubId(long subid, string plattform, MySqlTransaction transaction)
        {
            try
            {
                _getFileBySubId.Transaction = transaction;

                _getFileBySubId.Parameters.Clear();
                _getFileBySubId.Parameters.AddWithValue("subid", subid);
                _getFileBySubId.Parameters.AddWithValue("plattform", plattform);

                object id = _getFileBySubId.ExecuteScalar();
                if (id != null)
                {
                    return Convert.ToInt64(id);
                }
                return -1;
            }
            catch(Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
                return -1;
            }
        }
        /// <summary>
        /// Get Keyword Id by 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="transaction"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        private int getKeyword(string name, MySqlTransaction transaction, bool create = true)
        {
            try
            {
                //Get Id of the Keyword from the Database
                _getKeywordId.Parameters.Clear();
                _getKeywordId.Parameters.AddWithValue("keyword", name);

                object word = _getKeywordId.ExecuteScalar();
                //Is the Keyword already in the Databse
                if (word != null)
                {
                    //return Id from Database
                    return Convert.ToInt32(word);
                }
                else if (create)//Are we allowed to create 
                {
                    _insertKeyword.Transaction = transaction;
                    _insertKeyword.Parameters.Clear();
                    _insertKeyword.Parameters.AddWithValue("keyword", name);
                    if (_insertKeyword.ExecuteNonQuery() == 1)
                        return getKeyword(name, transaction, false); //Return new Id
                    throw new Exception();
                }
                return -1;
            }
            catch (Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
                return -1;
            }
        }
        /// <summary>
        /// Get Keyword for Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private string getKeyword(int id)
        {
            try
            {
                _getKeywordName.Parameters.Clear();
                _getKeywordName.Parameters.AddWithValue("id", id);

                object kid = _getKeywordName.ExecuteScalar();
                return kid.ToString();
            }
            catch (Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
                return "";
            }
        }
        /// <summary>
        /// Get User Id in Database
        /// </summary>
        /// <param name="userId">Numneric Id on the Plattform</param>
        /// <param name="plattform">The Plattform</param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        private int[] getUserIds(int[] userId, string[] names, string plattform, MySqlTransaction transaction)
        {
            try
            {
                List<int> ids = new List<int>();

                _getUserIds.Parameters.Clear();
                _getUserIds.Parameters.AddWithValue("userids", string.Join(',',userId));
                _getUserIds.Parameters.AddWithValue("plattform", plattform);

                var idsReader = _getUserIds.ExecuteReader();

                while(idsReader.Read())
                {
                    ids.Add(idsReader.GetInt32(0));
                }

                return ids.ToArray();
            }
            catch (Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
                return new int[0];
            }
        }

        private void updatePostSubmitters(long subid, int[] submitters)
        {
            foreach(int subm in submitters)
            {
                
            }
        }

        public Submitter GetSubmitterDetails(long submitterId)
        {
            return null;
        }
        public long GetSubmitter(string plattform, string name)
        {
            return 0;
        }
        private void updateUsersUsernames(int[] ids, string[] usernames, DateTime updated, MySqlTransaction transaction)
        {
            for(int i = 0; i < ids.Length; i++)
            {
                updateUserUsername(ids[i], usernames[i], updated, transaction);
            }
        }
        /// <summary>
        /// Update Username, after Username has Changed
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="username"></param>
        /// <param name="updated"></param>
        /// <param name="transaction"></param>
        private void updateUserUsername(int userid, string username, DateTime updated, MySqlTransaction transaction)
        {
            try
            {
                //Get Old Name
                var oldNames = getUserUsernames(userid);
                //Has his Name hasn't changed
                if (oldNames.Length > 0 && oldNames.Last().name == username)
                {
                    //Insert new Entry
                    _updateUsernameLastSeen.Transaction = transaction;
                    _updateUsernameLastSeen.Parameters.Clear();
                    _updateUsernameLastSeen.Parameters.AddWithValue("unid", oldNames.First().id);
                    _updateUsernameLastSeen.Parameters.AddWithValue("last_seen", updated);

                    _updateUsernameLastSeen.ExecuteNonQuery();
                }
                else//Add new Name to the Database
                {
                    _addUserUsername.Transaction = transaction;
                    _addUserUsername.Parameters.Clear();
                    _addUserUsername.Parameters.AddWithValue("user_id", userid);
                    _addUserUsername.Parameters.AddWithValue("username", username);
                    _addUserUsername.Parameters.AddWithValue("added", updated);
                    _addUserUsername.Parameters.AddWithValue("last_seen", updated);

                    _addUserUsername.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
            }
        }

        /// <summary>
        /// Get Name Entries for a Submitter
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        private NameDate[] getUserUsernames(int userid)
        {
            try
            {
                _getUserUsernames.Parameters.Clear();
                _getUserUsernames.Parameters.AddWithValue("user_id", userid);

                List<NameDate> names = new List<NameDate>();
                //Read all Entries
                var nameReader = _getUserUsernames.ExecuteReader();
                while (nameReader.Read())
                {
                    //Read Id
                    int id = nameReader.GetInt32(0);
                    //Read Name
                    string name = nameReader.GetValue(1).ToString();
                    //Read Last time seen
                    DateTime lastSeen = nameReader.GetDateTime(2);
                    names.Add(new NameDate() { name = name, lastSeen = lastSeen });
                }
                nameReader.Close();
                nameReader.Dispose();

                //Order Names by Last seen
                names.OrderByDescending(n => n.lastSeen);
                return names.ToArray();
            }
            catch (Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
                return new NameDate[0];
            }
        }

        /// <summary>
        /// Get all Submissions, associated with the given Keywords
        /// </summary>
        /// <param name="keywords"></param>
        /// <returns></returns>
        public long[] getSubmissionsForKeywords(string[] keywords)
        {
            List<long> posts = new List<long>();

            _getFilesWithKeywords.Parameters.Clear();
            _getFilesWithKeywords.Parameters.Add(new MySqlParameter("kws", string.Join(',', keywords)));
            _getFilesWithKeywords.Parameters.Add(new MySqlParameter("kwsc", keywords.Length));

            var reader = _getFileBySubId.ExecuteReader();
            while (reader.Read())
            {
                long id = reader.GetInt64(0);
                posts.Add(id);
            }

            return posts.ToArray();
        }
        /// <summary>
        /// Get the Details of all Submissions
        /// </summary>
        /// <param name="fileId"></param>
        /// <returns></returns>
        public SubmissionDetails[] GetSubmissionDetails(long[] fileId)
        {
            List<SubmissionDetails> submissions = new List<SubmissionDetails>();
            
            //Get all Submissions
            foreach(long id in fileId)
            {
                string plattform;
                string name;
                DateTime created;
                DateTime updated;
                string[] keywords = getAllKeywordsText(id);
                string path;
                SubmissionType type;
                Rating rating;

                _getSubDetails.Parameters.Clear();
                _getSubDetails.Parameters.Add(new MySqlParameter("subid", id));

                //Read all Submissions
                var reader = _getSubDetails.ExecuteReader();
                if(reader.Read())
                {
                    //SELECT plattform,subtype,name,created,updated,path,rating,submitter,description FROM files WHERE subid = @subid

                    //Read details
                    plattform = reader.GetString(0);
                    type = (SubmissionType)reader.GetInt32(1);
                    name = reader.GetString(2);
                    created = reader.GetDateTime(3);
                    updated = reader.GetDateTime(4);
                    path = reader.GetString(5);
                    rating = (Rating)reader.GetInt32(6);

                    //Add Submission to the List
                    submissions.Add(new SubmissionDetails(id, plattform, 0, created, updated, type, rating, keywords, path, null));
                }
            }

            //Return Submissions found
            return submissions.ToArray();
        }

        /// <summary>
        /// Close Connection to the Database
        /// </summary>
        public void Close()
        {
            try
            {
                _connection.Close();
                _connection.Dispose();

                _insertFile.Dispose();
            }
            catch (Exception e)
            {
                _log.Error(e.Message + e.StackTrace);
            }
        }

        public void Dispose()
        {
            Close();
        }

        public CollectionDetails GetCollectionDetails(long collectionId)
        {
            throw new NotImplementedException();
        }

        public void AddToCollection(long subid, long colid, long index)
        {
            throw new NotImplementedException();
        }

        public void RemoveFromCollection(long subid, long colid)
        {
            throw new NotImplementedException();
        }

        public SubmissionDetails GetSubmissionDetails(long submissionId)
        {
            throw new NotImplementedException();
        }

        public long[] GetFilesInCollection(long collectionId)
        {
            throw new NotImplementedException();
        }
    }
}
