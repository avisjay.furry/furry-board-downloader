﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace BoardCrawlerLib
{
    public class IBCrawler : IBoardAPI,IDisposable
    {
        private const string BOARD_ID = "IB";

        private ILog _log = LogManager.GetLogger(typeof(IBCrawler));

        private IDBConnector _db;
        private WebClient client = new WebClient();

        private string sessionId = "";
        private string userId = "";

        string IBoardAPI.storageTarget { get; set; }

        public IBCrawler(IDBConnector db)
        {
            _db = db;
        }        

        public bool Login(XmlNode config)
        {
            try
            {
                
                string username = config["Username"].InnerText;
                string password = config["Password"].InnerText;

                string loginString = "https://inkbunny.net/api_login.php?output_mode=xml&username=" + username + "& password=" + password;

                try
                {
                    string result;
                    if (IBoardAPI.SendRequest(loginString, out result))
                    {
                        XmlDocument response = new XmlDocument();
                        response.LoadXml(result);
                        sessionId = response.GetElementsByTagName("sid")?.Item(0).InnerText;
                        userId = response.GetElementsByTagName("user_id")?.Item(0).InnerText;

                        return true;
                    }
                }
                catch(Exception ex)
                {
                    _log.Error(ex.Message + ex.StackTrace);
                }

                return false;
            }
            catch(Exception ex)
            {
                _log.Error(ex.Message + ex.StackTrace);
                return false;
            }
        }

        public bool Logout()
        {
            try
            {
                string logoutString = "https://inkbunny.net/api_logout.php?outputmode=xml&sid=" + sessionId;
                string response = "";
                if (IBoardAPI.SendRequest(logoutString, out response))
                {
                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                _log.Error(ex.Message + ex.StackTrace);
                return false;
            }
        }

        public void Dispose()
        {
            if(!string.IsNullOrEmpty(sessionId))
            {
                Logout();
            }
        }

        public void DownloadFavs()
        {
            try
            {
                _log.Info("Start downloading favorites list");
                string favList;
                string request = "https://inkbunny.net/api_search.php?sid=" + sessionId + "&output_mode=xml&get_rid=yes&favs_user_id=" + userId + "&no_submissions=yes";
                _log.Info("Catch Search Id");
                if (IBoardAPI.SendRequest(request, out favList))
                {
                    XmlDocument favs = new XmlDocument();
                    favs.LoadXml(favList);

                    var count = favs.SelectNodes("//response/pages_count");
                    if (count.Count > 0)
                    {
                        var ridnode = favs.SelectNodes("//response/rid");
                        if (ridnode.Count > 0)
                        {
                            string rid = ridnode.Item(0).InnerText;
                            _log.Info("request id is " + rid);

                            int pageCount = Int32.Parse(count.Item(0).InnerText);
                            _log.Info("Found " + count + " Pages");
                            int currentPage = 0;

                            do
                            {
                                _log.Info("Read Page " + currentPage + 1);
                                string currentPageXml;
                                if (IBoardAPI.SendRequest("https://inkbunny.net/api_search.php?sid=" + sessionId + "&output_mode=xml&page=" + currentPage + "&rid=" + rid + "&no_submissions=no", out currentPageXml))
                                {
                                    XmlDocument page = new XmlDocument();
                                    page.LoadXml(currentPageXml);
                                    ParsePage(page);
                                }
                                currentPage++;
                            }
                            while (currentPage < pageCount);
                        }
                    }
                }

                _db.CleanUpKeywords();
            }
            catch(Exception ex)
            {
                _log.Error(ex.Message + ex.StackTrace);
            }
        }

        private void ParsePage(XmlDocument results)
        {
            _log.Info("Parse Page");
            // //response/submissions/submission

            var submissions = results.SelectNodes("//response/submissions/submission");
            foreach(XmlNode n in submissions)
            {
                long subId = Int64.Parse(n.SelectSingleNode("submission_id").InnerText);
                string userName = n.SelectSingleNode("username").InnerText;
                int user_id = Int32.Parse(n.SelectSingleNode("user_id").InnerText);

                _log.Info("Parse Submission " + subId);

                SubmissionType submissionType = translateSubmissionType(Int32.Parse(n.SelectSingleNode("submission_type_id").InnerText));
                string name = n.SelectSingleNode("title").InnerText;

                DateTime created = DateTimeParser.parse(n.SelectSingleNode("create_datetime_usertime").InnerText);
                DateTime updated = DateTimeParser.parse(n.SelectSingleNode("last_file_update_datetime_usertime").InnerText);

                Rating rating = translateRating(Int32.Parse(n.SelectSingleNode("rating_id").InnerText));

                Dictionary<string,string> files;//Download and Fill
                string[] keywords;
                string description;
                getSubmissionData(subId, out files, out keywords, out description);

                _log.Info("Get Storage Path");
                string localPath = ((IBoardAPI)this).getStoragePath(name, subId);
                _log.Info("Update Database");
                long nid;
                if (_db.InsertSubmission(subId, BOARD_ID, submissionType, name, updated, created, localPath, rating, new int[] { user_id }, new string[]{ userName }, description, keywords, out nid) || !Directory.Exists(localPath))
                {
                    try
                    {
                        _log.Info("Create Directory " + localPath);
                        if (!Directory.Exists(localPath))
                            Directory.CreateDirectory(localPath);

                        download(files, localPath);
                    }
                    catch(Exception e)
                    {
                        _log.Warn("Download Failed " + e.Message + e.StackTrace);
                    }
                }
                else
                {
                    _log.Info("File up to date!");
                }
            }
        }

        private bool getSubmissionData(long subId, out Dictionary<string,string> filePath, out string[] keywords, out string description)
        {
            _log.Info("Get Additional Data");
            string subXml;
            description = "";

            string requestString = "https://inkbunny.net/api_submissions.php?sid=" + sessionId + "&output_mode=xml&submission_ids=" + subId + "&show_description=yes" /*+ "&show_description_bbcode_parsed=yes"*/;
            if (IBoardAPI.SendRequest(requestString, out subXml))
            {
                List<string> kws = new List<string>();
                filePath = new Dictionary<string, string>();
                XmlDocument subData = new XmlDocument();
                subData.LoadXml(subXml);

                var SubNode = subData.GetElementsByTagName("submission");
                if (SubNode.Count > 0)
                {
                    _log.Info("Get Keywords");
                    var submissionNode = SubNode.Item(0);
                    foreach (var keyword in submissionNode.SelectNodes("keywords/keyword"))
                    {
                        string name = ((XmlNode)keyword).SelectSingleNode("keyword_name").InnerText;
                        kws.Add(name);
                    }
                    keywords = kws.ToArray();

                    _log.Info("Get File List");
                    foreach (var file in submissionNode.SelectNodes("files/file"))
                    {
                        string fileName = ((XmlNode)file).SelectSingleNode("file_name").InnerText;
                        string url = ((XmlNode)file).SelectSingleNode("file_url_full").InnerText;
                        filePath.Add(fileName, url);
                    }

                    //var descNode = submissionNode.SelectSingleNode("description_bbcode_parsed");
                    var descNode = submissionNode.SelectSingleNode("description");
                    if (descNode != null)
                        description = descNode.InnerText;
                }
                else
                    keywords = new string[0];

                return true;
            }

            filePath = new Dictionary<string, string>();
            keywords = new string[0];
            description = "";
            return false;
        }
        private string download(Dictionary<string, string> files, string path)
        {
            foreach(KeyValuePair<string,string> file in files)
            {
                _log.Info("Dowload File " + file.Key);
                client.DownloadFile(file.Value, Path.Combine(path, file.Key));
            }

            return path;
        }

        private Rating translateRating(int id)
        {
            switch(id)
            {
                case 0:
                    return Rating.Safe;

                case 1:
                    return Rating.Explicit;

                case 2:
                    return Rating.Extreme;

                default:
                    return Rating.Safe;
            }
        }

        private SubmissionType translateSubmissionType(int id)
        {
            switch(id)
            {
                case 1:
                case 2:
                case 3:
                case 13:
                case 14:
                    return SubmissionType.Image;

                case 4:
                    return SubmissionType.Comic;

                case 5:
                    return SubmissionType.Other;

                case 6:
                case 7:
                case 8:
                case 9:
                    return SubmissionType.Video;

                case 12:
                    return SubmissionType.Story;

                default:
                    return SubmissionType.Other;
            }
        }
    }
}
