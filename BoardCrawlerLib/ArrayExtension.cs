﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BoardCrawlerLib
{
    static class ArrayExtension
    {
        public static bool Compare<T>(this T[] a1, T[] a2)
        {
            if (a1.Length != a2.Length)
                return false;
            
            for(int i = 0; i < a1.Length; i++)
            {
                if (!a1[i].Equals(a2[i]))
                    return false;
            }
            return true;
        }    
    }
}
