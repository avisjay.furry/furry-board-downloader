﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;

namespace BoardCrawlerLib
{
    

    public enum SubmissionType { Image, Comic, Story, Journal, Video, Other }
    public enum Rating { Safe, Questionable, Explicit, Extreme }

    /// <summary>
    /// Base for an Interface to a Plattform
    /// </summary>
    public interface IBoardAPI
    {
        const string UserAgentName = "BoardCrawler (by Avis Jay on e621 and Sofurry)";

        static int retrys = 5;
        
        /// <summary>
        /// Path to Store the Images to
        /// </summary>
        public string storageTarget { protected get; set; }

        /// <summary>
        /// Login to a Plattform
        /// </summary>
        /// <param name="creds"></param>
        /// <returns></returns>
        public bool Login(XmlNode creds);
        /// <summary>
        /// Logout from a Plattform
        /// </summary>
        /// <returns></returns>
        public bool Logout();
        /// <summary>
        /// Download Favourites
        /// </summary>
        public void DownloadFavs();

        /// <summary>
        /// Request Page from Api
        /// </summary>
        /// <param name="url">Url to request</param>
        /// <param name="resp">Output</param>
        /// <param name="trys">Times Retried</param>
        /// <param name="creds"></param>
        /// <returns></returns>
        protected static bool SendRequest(string url, out string resp, int trys = 0, CredentialCache creds = null)
        {
            ILog log = LogManager.GetLogger(typeof(IBoardAPI));

            try
            {
                //Create Request for the URL
                WebRequest request = WebRequest.Create(url);
                //Have we Credentials
                if (creds != null)
                {
                    request.Credentials = creds;
                    request.PreAuthenticate = true;
                }

                //Set Useragent
                ((HttpWebRequest)request).UserAgent = UserAgentName;

                //Send Request
                WebResponse response = request.GetResponse();

                //Was the Request Successfull
                if (((HttpWebResponse)response).StatusCode == HttpStatusCode.OK)
                {
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        //Read Response
                        StreamReader reader = new StreamReader(dataStream);
                        resp = reader.ReadToEnd();
                        return true;
                    }
                }

                //Request Failed
                resp = "";
                return false;
            }
            catch(Exception e)
            {
                log.Warn(e.Message + e.StackTrace);
                //Have we sent too many Requests?
                if (e.Message.Contains("(429) Too Many Requests"))
                {
                    //Retry only the set amount of times
                    if (trys > retrys)
                        throw new TooManyRequestsException();
                    //Wait 5 seconds between retries
                    Thread.Sleep(new TimeSpan(0, 1, 0));
                    //Retry
                    return SendRequest(url, out resp, trys + 1, creds);
                }
                throw e;
            }
        }

        /// <summary>
        /// Get path to Store file
        /// </summary>
        /// <param name="title"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public string getStoragePath(string title, long id)
        {
            return Path.Combine(storageTarget, id + "_" + SanatizeString(title));
        }

        /// <summary>
        /// Remove Illegal Characters from a Name, before creating a String from it
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        protected string SanatizeString(string s)
        {
            string[] additionalChars = { "_", " ", "/", "\\" };

            string newS = "";
            foreach (char c in s)
            {
                if (char.IsLetter(c) || char.IsDigit(c) || additionalChars.Contains("" + c))
                {
                    newS += c;
                }
            }

            while (newS.Contains("  "))
                newS = newS.Replace("  ", " ");

            newS = newS.TrimStart().TrimEnd();

            return newS;
        }
    }

    class TooManyRequestsException:Exception
    {
    }
}
