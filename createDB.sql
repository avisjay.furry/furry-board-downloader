-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.5.8-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win64
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Exportiere Struktur von Tabelle crawlerdb.blockedsubmissions
CREATE crawlerdbTABLE IF NOT EXISTS `blockedsubmissions` (
  `subid` int(10) unsigned zerofill NOT NULL,
  `plattform` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

-- Exportiere Struktur von Tabelle crawlerdb.collectionfiles
CREATE TABLE IF NOT EXISTS `collectionfiles` (
  `Collection` int(10) unsigned NOT NULL,
  `File` int(11) unsigned NOT NULL,
  KEY `FK__postcollection` (`Collection`),
  KEY `File` (`File`),
  CONSTRAINT `FK__postcollection` FOREIGN KEY (`Collection`) REFERENCES `postcollection` (`CollectionId`),
  CONSTRAINT `File` FOREIGN KEY (`File`) REFERENCES `files` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

-- Exportiere Struktur von Tabelle crawlerdb.collectiontypes
CREATE TABLE IF NOT EXISTS `collectiontypes` (
  `TypeId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  KEY `Main` (`TypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

-- Exportiere Struktur von Tabelle crawlerdb.files
CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `subid` int(10) unsigned zerofill NOT NULL,
  `plattform` varchar(2) DEFAULT NULL,
  `submtype` int(11) DEFAULT NULL,
  `name` char(50) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `path` varchar(500) NOT NULL,
  `rating` char(2) NOT NULL,
  `submitter` int(11) NOT NULL,
  `description` mediumtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_files_user` (`submitter`),
  CONSTRAINT `FK_files_user` FOREIGN KEY (`submitter`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=483 DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

-- Exportiere Struktur von Tabelle crawlerdb.file_keywords
CREATE TABLE IF NOT EXISTS `file_keywords` (
  `file_id` int(11) unsigned NOT NULL,
  `keyword_id` int(11) unsigned NOT NULL,
  KEY `FK_file_keywords_files` (`file_id`),
  KEY `FK_file_keywords_keywords` (`keyword_id`),
  CONSTRAINT `FK_file_keywords_files` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`),
  CONSTRAINT `FK_file_keywords_keywords` FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`kwid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

-- Exportiere Struktur von Tabelle crawlerdb.joinedusers
CREATE TABLE IF NOT EXISTS `joinedusers` (
  `FirstId` int(11) unsigned zerofill NOT NULL,
  `SecondId` int(11) unsigned zerofill NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

-- Exportiere Struktur von Tabelle crawlerdb.keywords
CREATE TABLE IF NOT EXISTS `keywords` (
  `kwid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kwid`),
  UNIQUE KEY `keyword` (`keyword`)
) ENGINE=InnoDB AUTO_INCREMENT=3871 DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

-- Exportiere Struktur von Tabelle crawlerdb.postcollection
CREATE TABLE IF NOT EXISTS `postcollection` (
  `CollectionId` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `Plattform` varchar(2) NOT NULL,
  `CollectionType` int(11) NOT NULL DEFAULT 0,
  `CollectionDescription` longtext NOT NULL,
  `CollectionName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`CollectionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

-- Exportiere Struktur von Tabelle crawlerdb.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `plattform` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

-- Exportiere Struktur von Tabelle crawlerdb.usernames
CREATE TABLE IF NOT EXISTS `usernames` (
  `username_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `added` datetime DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  PRIMARY KEY (`username_id`),
  KEY `FK_usernames_user` (`user_id`),
  CONSTRAINT `FK_usernames_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;

-- Daten Export vom Benutzer nicht ausgewählt

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
